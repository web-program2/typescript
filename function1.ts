// Return Type
function getTime(): number {
    return new Date().getTime();
}

console.log(getTime());

//Void Return Type
function printHello(): void {
    console.log("Hello")
}

printHello();

//Parameters
function multiply(a: number, b: number): number {
    return a * b;
}

console.log(multiply(1, 2));

//Optional Parameters
function add(a: number, b: number, c?: number) {
    return a + b + (c || 0);
}

console.log(add(1, 2, 3));
console.log(add(1, 2));

//Default Parameters
function pow(value: number, exponent: number = 10) {
    return value ** exponent;
}

console.log(pow(10));
console.log(pow(10, 2));

//Named Parameters
function divide({ dividend, divisor }: { dividend: number, divisor: number }) {
    return dividend / divisor;
}

console.log(divide({ dividend: 100, divisor: 10 }));

// Rest Parameters
function add2(a: number, b: number, ...rest: number[]) {
    return a + b + rest.reduce((p, c) => p + c, 0);
}

console.log(add2(1, 2, 3, 4, 5));

//Type Alias
type Negate = (value: number) => number;

const negateFunction: Negate = (value) => value * -1;
const negateFunction2: Negate = function (value: number) { return value * -1 };

console.log(negateFunction(1));
console.log(negateFunction2(1));