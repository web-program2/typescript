let w: unknown = 1;
w = "string";
w = {
    runAnonExistentMethod: () => {
        console.log("I Think Therefore I Am ");
    }
} as { runAnonExistentMethod: () => void }

if(typeof w === 'object' && w!== null){
    (w as {runAnonExistentMethod: Function }).runAnonExistentMethod();
}